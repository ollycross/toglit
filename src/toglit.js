class Toglit {
  constructor(selector = 'body', options) {

    this.selector = selector;
    this.element = null;

    this.options = Object.assign({}, {
      enabled: true,
      defaultToggleClass: 'in',
      defaultTriggers: 'click',
      dataToggleKey: 'toglit',
      dataClassKey: 'toglitClass',
      dataTriggersKey: 'toglitTriggers',
    }, options);

    this.handleLoaded = this.handleLoaded.bind(this);
    this.handleToggleTrigger = this.handleToggleTrigger.bind(this);

    if (/comp|inter|loaded/.test(document.readyState)) {
      this.handleLoaded();
    } else {
      document.addEventListener('DOMContentLoaded', this.handleLoaded);
    }

    return {
      enable: this.enable,
      disable: this.disable,
      enabled: this.enabled,
    }
  }

  enable() {
    this.options.enabled = true;
    return this;
  };

  disable() {
    this.options.enabled = false;
    return this;
  }

  enabled() {
    return this.options.enabled === true;
  }

  toggleClasses(item) {
    const {
      element,
    } = this;
    const {
      defaultToggleClass,
      dataToggleKey,
      dataClassKey
    } = this.options;
    const {
      dataset
    } = item;
    const targets = element.querySelectorAll(dataset[dataToggleKey]);
    const toggleClasses = (dataset[dataClassKey] || defaultToggleClass).split(" ");

    for (let i = targets.length - 1; i >= 0; i--) {
      for (let x = toggleClasses.length - 1; x >= 0; x--) {
        this.toggleClass(targets[i], toggleClasses[x]);
      }
    }
  }

  toggleClass(target, theClass) {
    const method = (target.classList.contains(theClass)) ? 'remove' : 'add';
    target.classList[method].call(target.classList, theClass);
    this.fireEvent(target, [method], {
      class: theClass,
    });
    this.fireEvent(target, 'change', {
      change: method,
      class: theClass,
    });
  }

  fireEvent(element, labels = [], data = {}) {
    if (!Array.isArray(labels)) {
      labels = [labels];
    }
    let label;
    for (let i = labels.length - 1; i >= 0; i--) {
      label = labels[i];
      const event = new CustomEvent(`toglit${label}`, {
        bubbles: true,
        detail: data,
      });
      element.dispatchEvent(event);
    }
  }

  handleToggleTrigger(e) {
    if (this.enabled()) {
      e.preventDefault();
      const {
        currentTarget,
      } = e;
      this.toggleClasses(currentTarget);
    }
  }

  handleLoaded() {
    const {
      selector,
    } = this;
    const {
      defaultTriggers,
      dataTriggersKey,
      dataToggleKey,
    } = this.options;
    this.element = typeof this.selector === 'string' ? document.querySelector(this.selector) : this.selector;
    if (this.element) {
      const elements = this.element.querySelectorAll(`[data-${dataToggleKey}]`);
      for (let i = elements.length - 1; i >= 0; i--) {
        const triggers = (elements[i].dataset[dataTriggersKey] || defaultTriggers).split(" ");
        for (let x = triggers.length - 1; x >= 0; x--) {
          elements[i].addEventListener(triggers[x], this.handleToggleTrigger);
        }
      }
    }
  }
}

export default Toglit;
