
# Toglit!
A pure javascript utility to easily toggle classes on elements targeted by data attributes

## Usage

    import Toglit from 'toglit'
    const toglit = new Toglit();
    
    window.addEventListener('toglitchange', (e) => {
        const { change, class } = e.detail;
        const { currentTarget } = e;
        console.log(`${change}ed ".${class}" "${from}" to "#${currentTarget.id}!"`);
        // Added ".in" to "#toggleMe!"
    });
    
    <style>
        #toggleme {
            display:none;
        }   
        #toggleme.in {
            display:block;
        }
    </style>

    <button type="button" data-toglit="#toggleme">Toglit!</button>
    <div id="toggleme">Lookit me!</div>

## Data attributes
These should be set on the firing element (e.g. a button) and will be activated on click.  Note that their keys can be changed using the options object (see below)
__toglit__ _string|element_ _required_ The target element 

__toglit-class__ _string_ The class(es) to be toggled on the target element

__toglit-triggers__ _string_ The trigger(s) to be toggled on the target element


## Arguments
__selector__ _string|element_ The container for toglit to work within. 
_default_: `body`

__options__ _object_ Options object (see below)

## Options

 __enabled__ _boolean_ If `false` the will disable all Toglit functionality.
 _default_: `true`

__defaultToggleClass__ _string_ The class(es) to be toggled on the target element _if_ no `data-togglit-class` is provided.
_default_: `in`

__defaultTriggers__ _string_ The trigger(s) to be toggled on the target element _if_ no `data-toglit-triggers` is provided.
_default_: `in`

 __dataToggleKey__ _string_ The `data` attribute on the firing element that holds the query selector.
_default_: `toglit`

 __dataClassKey__ _string_ The `data` attribute on the firing element that holds the classes to toggle.
_default_: `toglit-class`

 __dataTriggersKey__ _string_ The `data` attribute on the firing element that holds the triggers on which to toggle.
_default_: `toglit-triggers`
    
## Methods
__enable ()__ _Toglit_  Enables the toglit

__disable ()__ _Toglit_  Disables the toglit

__enabled ()__ _boolean_ returns `true` if the toglit is enabled

## Events
__toglitchange__ Fires when the class is added _or_ removed from the target element.  `event.detail` carries an object containing `change` (either `add` or `remove`) and `class`, the class which was changed.

__toglitadd__ Fires when the class is added to the target element.  `event.detail` carries an object containing `class`, the class which was added.

__toglitadd__ Fires when the class is removed from the target element.  `event.detail` carries an object containing `class`, the class which was removed.

_NB: If using jQuery you may need to look at event.originalEvent.detail_

## Notes
- The `data-toggle` attribute can be any valid CSS selector.  Internally Toglit uses `querySelectorAll` to find and iterate through targets.  If more than one target is found, the class will be toggled on each item individually.
- `data-class` and `defaultToggleClass` can contain mutiple, space separated classes.  Each will be toggled individually.
- Specifiying an element in the constructor of Toglit will limit all behaviour to elements within that element.
- This package is designed to be used with webpack and babel.  The `main` entry in package json points to the ES6 module in `/src`.  If this causes problems with your build process you can find the transpiled and bundled code in the `/dist` folder.